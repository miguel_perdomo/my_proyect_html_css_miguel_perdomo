# PROYECTO FINAL HTML - CSS

**Enhorabuena** por haber finalizdo el módulo de HTML5 y CSS3. Sin duda, no eres el mismo programador que hace una semana ¿Verdad? :wink:


Ahora ha llegado el momento de poner a prueba en el proyecto final del módulo, tus habilidades como Jedi.

El reto consiste en clonar la página `Home` de Upgrade Hub.
> https://upgrade-hub.com/

### Vamos por pasos jóven padawan. Esto te vas a encontrar

En este proyecto encontrarás una carpeta llamada `images`. Dentro de la carpeta encontrarás todos las imágenes y el vídeo que necesitas. Si no encuentras alguna, siempre puedes descargarla directamente de la página.

Aquí tienes la tipografía
> https://fonts.google.com/specimen/Work+Sans?preview.text_type=custom

# Requisitos
Nos centraremos en copiar la estructura en HTML y el estilo en CSS, para ello necesitaremos cumplir los siguientes requisitos

- Código HTML organizado jerárquicamente y cumplir un uso correcto de las etiquetas semánticas
- Añadir estilos en CSS (TIP: Puedes añadir varias hojas de estilos)
- Usa BEM para poner nombre a las clases de tus elementos
- Maquetación Desktop
- Maquetación Mobile
- Commits constantes con cada funcionalidad


## BONUS
¿La fuerza te acompaña? Utilízala para completar los bonus
- Adapta tu diseño a Tablet
- Transiciones y animaciones. Te invitamos a crear animaciones como las de nuestra web o a desarrollar las tuyas propias (puedes usar una librería de animaciones)
- Clona otra página de la web Upgradeo Hub y enlaza ambas páginas


# Entrega
Puedes ir haciendo subidas a Gitlab mientras programas, cuando hayas finalizado el proyecto, sube a Gitlab el código y envía un mail a tu mentor con el enlace al repositorio.


# Desktop
![Desktop](images/screenshots/desktop.png)

# Tablet
![Desktop](images/screenshots/tablet.png)

# Mobile
![Desktop](images/screenshots/mobile.png)

